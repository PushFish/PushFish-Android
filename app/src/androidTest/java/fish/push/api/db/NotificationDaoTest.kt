package fish.push.api.db

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import fish.push.api.getOrAwaitValue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@SmallTest
class NotificationDaoTest {
    @get:Rule
    var instantTaskExecutorRunWith = InstantTaskExecutorRule()

    private lateinit var database: PushFishDatabase
    private lateinit var dao: NotificationDao

    @Before
    fun setup() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            PushFishDatabase::class.java
        ).allowMainThreadQueries().build()
        dao = database.getNotificationDao()
    }
    @After
    fun teardown() {
        database.close()
    }

    @Test
    fun addNotification() = runBlockingTest {
        val note = Notification("https://push.fish/img/pushfish.svg", 1, "PushFish", "test", "my message" )
        note.id = dao.addNotification(note)
        val allNotes = dao.filterBy("timestamp").getOrAwaitValue()
        assertThat(allNotes).contains(note)
        assertThat(allNotes.size).isEqualTo(1)
    }

    @Test
    fun addMultipleNotification() = runBlockingTest {
        val note = Notification("https://push.fish/img/pushfish.svg", 1, "PushFish", "test", "my message" )
        val note2 = Notification("https://push.fish/img/pushfish.svg", 2, "PushFish", "test", "my message" )
        note.id = dao.addNotification(note)
        note2.id = dao.addNotification(note2)
        val allNotes = dao.filterBy("timestamp").getOrAwaitValue()
        assertThat(allNotes).contains(note)
        assertThat(allNotes).contains(note2)
        assertThat(allNotes.size).isEqualTo(2)
    }

    @Test
    fun deleteNotification() = runBlockingTest {
        val note = Notification("https://push.fish/img/pushfish.svg", 1, "PushFish", "test", "my message" )
        note.id = dao.addNotification(note)
        dao.deleteNotification(note)
        val allNotes = dao.filterBy("timestamp").getOrAwaitValue()
        assertThat(allNotes).doesNotContain(note)
    }

    @Test
    fun deleteMultipleNotification() = runBlockingTest {
        val note = Notification("https://push.fish/img/pushfish.svg", 1, "PushFish", "test", "my message" )
        val note2 = Notification("https://push.fish/img/pushfish.svg", 2, "PushFish", "test", "my message" )
        note.id = dao.addNotification(note)
        note2.id = dao.addNotification(note2)

        // Test deletion of first notification
        dao.deleteNotification(note)
        var allNotes = dao.filterBy("timestamp").getOrAwaitValue()
        assertThat(allNotes).doesNotContain(note)
        assertThat(allNotes.size).isEqualTo(1)

        // Test deletion of second notification
        dao.deleteNotification(note2)
        allNotes = dao.filterBy("timestamp").getOrAwaitValue()
        assertThat(allNotes).doesNotContain(note2)
        assertThat(allNotes.size).isEqualTo(0)
    }

    @Test
    fun numberOfNotifications() = runBlockingTest {
        val note = Notification("https://push.fish/img/pushfish.svg", 1, "PushFish", "test", "my message" )
        val note2 = Notification("https://push.fish/img/pushfish.svg", 2, "PushFish", "test", "my message" )

        note.id = dao.addNotification(note)
        var numNotes = dao.numberOfNotifications()
        assertThat(numNotes).isEqualTo(1)

        note2.id = dao.addNotification(note2)
        numNotes = dao.numberOfNotifications()
        assertThat(numNotes).isEqualTo(2)

        dao.deleteNotification(note)
        numNotes = dao.numberOfNotifications()
        assertThat(numNotes).isEqualTo(1)

        dao.deleteNotification(note2)
        numNotes = dao.numberOfNotifications()
        assertThat(numNotes).isEqualTo(0)
    }
}