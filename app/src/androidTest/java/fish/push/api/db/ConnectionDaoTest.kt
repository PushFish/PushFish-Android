package fish.push.api.db
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import fish.push.api.getOrAwaitValue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@SmallTest
class ConnectionDaoTest {
    @get:Rule
    var instantTaskExecutorRunWith = InstantTaskExecutorRule()

    private lateinit var database: PushFishDatabase
    private lateinit var dao: ConnectionDao

    @Before
    fun setup() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            PushFishDatabase::class.java
        ).allowMainThreadQueries().build()
        dao = database.getConnectionDao()
    }
    @After
    fun teardown() {
        database.close()
    }

    @Test
    fun addConnection() = runBlockingTest {
        val connection = Connection()
        dao.addConnection(connection)
        val allConnections = dao.getConnections().getOrAwaitValue()
        assertThat(allConnections).contains(connection)
    }

    @Test
    fun addMultipleConnection() = runBlockingTest {
        val connection = Connection(host = "server1.test")
        val secondConnection = Connection(host = "server2.test")

        // Add first connection / test
        connection.id = dao.addConnection(connection)
        println("1st: ${connection.id}")
        var allConnections = dao.getConnections().getOrAwaitValue()
        assertThat(allConnections).contains(connection)

        // Add second connection / test
        secondConnection.id = dao.addConnection(secondConnection)
        println("2nd: ${secondConnection.id}")
        allConnections = dao.getConnections().getOrAwaitValue()
        assertThat(allConnections).contains(secondConnection)

        assertThat(allConnections.size).isEqualTo(2)
    }

    @Test
    fun deleteConnection() = runBlockingTest {
        val connection = Connection()
        connection.id = dao.addConnection(connection)
        dao.deleteConnection(connection)
        val allConnections = dao.getConnections().getOrAwaitValue()
        assertThat(allConnections).doesNotContain(connection)
    }

    @Test
    fun deleteMultipleConnection() = runBlockingTest {
        val connection = Connection(host = "server1.test")
        val secondConnection = Connection(host = "server2.test")

        // Add both connections
        connection.id = dao.addConnection(connection)
        println("1st: ${connection.id}")
        secondConnection.id = dao.addConnection(secondConnection)
        println("2nd: ${secondConnection.id}")

        dao.deleteConnection(secondConnection)
        var allConnections = dao.getConnections().getOrAwaitValue()
        assertThat(allConnections).doesNotContain(secondConnection)
        assertThat(allConnections.size).isEqualTo(1)

        dao.deleteConnection(connection)
        allConnections = dao.getConnections().getOrAwaitValue()
        assertThat(allConnections).doesNotContain(connection)
        assertThat(allConnections.size).isEqualTo(0)
    }
}