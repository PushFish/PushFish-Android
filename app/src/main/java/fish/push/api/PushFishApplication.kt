package fish.push.api

import android.app.Application
import dagger.hilt.android.HiltAndroidApp
import fish.push.api.internal.Connections
import timber.log.Timber

@HiltAndroidApp
class PushFishApplication : Application() {
    companion object {
        lateinit var app: PushFishApplication
        val connections by lazy {
            Connections()
        }
    }

    override fun onCreate() {
        super.onCreate()
        app = this

        Timber.plant(Timber.DebugTree())
    }

}