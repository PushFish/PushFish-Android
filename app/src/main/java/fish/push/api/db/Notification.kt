package fish.push.api.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "notifications")
data class Notification(
    var imageURL: String = "",
    var timestamp: Long = 0L,
    var server: String = "",
    var topic: String = "",
    var message: String = ""
)
{
    @PrimaryKey(autoGenerate = true) var id: Long = 0
}
