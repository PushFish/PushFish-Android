package fish.push.api.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "subscriptions")
data class Subscription (
    var connectionId: Long,
    var clientHandle: String = "",
    var topic: String = "",
    var notify: Int = 0,
    var QoS: Int = 0,
    @PrimaryKey(autoGenerate = true) var id: Long = 0
)
{
}
