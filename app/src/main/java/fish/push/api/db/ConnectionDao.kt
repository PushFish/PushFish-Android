package fish.push.api.db

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface ConnectionDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addConnection(connection: Connection) : Long

    @Delete
    suspend fun deleteConnection(connection: Connection)

    @Query(" SELECT * FROM connections ")
    fun getConnections() : LiveData<List<Connection>>

    @Transaction
    @Query("SELECT * FROM subscriptions WHERE connectionId LIKE :connectionId")
    fun getSubscriptions(connectionId: Long) : LiveData<List<Subscription>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun subscribe(subscription: Subscription) : Long

    @Transaction
    suspend fun subscribe(connection: Connection, subscription : Subscription): Long {
        val connectionId = connection.id
        subscription.connectionId = connectionId
        return subscribe(subscription)
    }

    @Delete
    suspend fun unsubscribe(subscription: Subscription)

}