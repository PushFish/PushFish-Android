package fish.push.api.db

import androidx.room.TypeConverter
import com.google.gson.Gson

class Converters {
    @TypeConverter
    fun listToJson(value: List<Subscription>?): String {

        return Gson().toJson(value)
    }

    @TypeConverter
    fun jsonToList(value: String): List<Subscription>? {

        val objects = Gson().fromJson(value, Array<Subscription>::class.java )
        return objects.toList()
    }
}