package fish.push.api.db

import androidx.lifecycle.LiveData
import androidx.room.Embedded
import androidx.room.Relation

data class ConnectionWithSubscriptions (
    @Embedded val connection: Connection,
    @Relation(
        parentColumn = "id",
        entityColumn = "connectionId"
    )
    val subscriptions: LiveData<List<Subscription>>
)
