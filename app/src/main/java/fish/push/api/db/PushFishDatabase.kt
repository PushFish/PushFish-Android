package fish.push.api.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import fish.push.api.internal.Connections

@Database(
    entities = [Connection::class, Notification::class, Subscription::class],
    version = 1
)
@TypeConverters(Converters::class)
abstract class PushFishDatabase : RoomDatabase() {
    abstract fun getNotificationDao(): NotificationDao
    abstract fun getConnectionDao(): ConnectionDao
}