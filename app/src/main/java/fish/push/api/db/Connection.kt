package fish.push.api.db

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "connections")
data class Connection (
    var clientHandle: String = "myClientHandle",
    var host: String = "mqtt.eclipse.org",
    var displayName: String = "",
    var imageURL: String = "https://push.fish/img/pushfish.svg",
    var clientID: String = "myClientID",
    var port: Int = 1883,
    var ssl: Int = 0,
    var timeout: Int = 30,
    var keepAlive: Int = 90,
    var username: String = "",
    var password: String = "",
    var cleanSession: Int = 1,
    var lwtTopic: String = "",
    var lwtMessage: String = "",
    var lwtQoS: Int = 0,
    var lwtColumnRetained: Int = 0,
    var shouldConnect: Boolean = true, // Handled by user toggle
    var isConnected: Boolean = false // Handled by server/client logic
) : Serializable {
    @PrimaryKey(autoGenerate = true) var id: Long = 0
}
