package fish.push.api.db

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface NotificationDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addNotification(notification: Notification) : Long

    @Delete
    suspend fun deleteNotification(notification: Notification)

    @Query("""

        SELECT * FROM notifications

        ORDER BY 

        CASE WHEN :column = 'timestamp'  THEN timestamp END DESC,

        CASE WHEN :column = 'server' THEN server END DESC,
        CASE WHEN :column = 'topic' THEN topic END DESC

    """)
    fun filterBy(column : String) : LiveData<List<Notification>>

    @Query("SELECT count(id) FROM notifications")
    suspend fun numberOfNotifications() : Int
}