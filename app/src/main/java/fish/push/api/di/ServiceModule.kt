package fish.push.api.di

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ServiceComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.scopes.ServiceScoped
import fish.push.api.Constants
import fish.push.api.Constants.NOTIFICATION_CHANNEL_ID
import fish.push.api.R
import fish.push.api.ui.MainActivity

@Module
@InstallIn(ServiceComponent::class)
object ServiceModule {

    @ServiceScoped
    @Provides
    fun provideMainActivityPendingIntent(
        @ApplicationContext app: Context
    ) = PendingIntent.getActivity(
        app,
        0,
        Intent(app, MainActivity::class.java).also {
        it.action = Constants.ACTION_SHOW_NOTIFICATION_FRAGMENT
    },
    PendingIntent.FLAG_UPDATE_CURRENT
    )

    @ServiceScoped
    @Provides
    fun provideBaseNotificationBuilder(
        @ApplicationContext app: Context,
        pendingIntent: PendingIntent
    ) = NotificationCompat.Builder(app, NOTIFICATION_CHANNEL_ID)
    .setSmallIcon(R.drawable.ic_launcher)
    .setContentTitle("PushFish Android")
    .setContentText("Listening for messages...")
    .setContentIntent(pendingIntent)
}