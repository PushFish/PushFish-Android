package fish.push.api.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import fish.push.api.Constants.NOTIFICATION_DATABASE_NAME
import fish.push.api.db.PushFishDatabase
import fish.push.api.internal.Connections
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideNotificationDatabase(
        @ApplicationContext app: Context
    ) = Room.databaseBuilder(
        app,
        PushFishDatabase::class.java,
        NOTIFICATION_DATABASE_NAME
    ).build()

    @Singleton
    @Provides
    fun provideNotificationDao(db: PushFishDatabase) = db.getNotificationDao()

    @Singleton
    @Provides
    fun provideConnectionDao(db: PushFishDatabase) = db.getConnectionDao()
}