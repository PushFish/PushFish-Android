package fish.push.api.internal

import fish.push.api.db.Connection
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

/**
 * `Connections` is a singleton class which stores all the connection objects
 * in one central place so they can be passed between activities using a client handle.
 */
@Singleton
class Connections @Inject constructor(){
    /**
     * List of [Connection] object
     */
    private var connections: HashMap<String, Connection> = HashMap<String, Connection>()

    /**
     * Finds and returns a [Connection] object that the given client handle points to
     *
     * @param handle The handle to the [Connection] to return
     * @return a connection associated with the client handle, `null` if one is not found
     */
    fun getConnection(handle: String): Connection? {
        return connections[handle]
    }

    /**
     * Adds a [Connection] object to the collection of connections associated with this object
     *
     * @param connection [Connection] to add
     */
    fun addConnection(connection: Connection) {
        connections[connection.clientHandle] = connection
    }
    /**
     * Get all the connections associated with this `Connections` object.
     *
     * @return `Map` of connections
     */
    fun getConnections(): Map<String, Connection> {
        return connections
    }

    /**
     * Removes a connection from the map of connections
     *
     * @param connection connection to be removed
     */
    fun removeConnection(connection: Connection) {
        connections.remove(connection.clientHandle)
    }

    /**
     * Updates an existing connection within the map of
     * connections as well as in the persisted model
     *
     * @param connection connection to be updated.
     */
    fun updateConnection(connection: Connection) {
        connections[connection.clientHandle] = connection
    }

}
