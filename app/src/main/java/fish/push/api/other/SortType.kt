package fish.push.api.other

enum class SortType {
    TIMESTAMP, SERVER, TOPIC
}