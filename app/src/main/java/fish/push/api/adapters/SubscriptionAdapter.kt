package fish.push.api.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import fish.push.api.R
import fish.push.api.db.Subscription
import kotlinx.android.synthetic.main.item_subscription.view.*

class SubscriptionAdapter: RecyclerView.Adapter<SubscriptionAdapter.SubscriptionViewHolder>(){
    class SubscriptionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    private val differCallback = object : DiffUtil.ItemCallback<Subscription>() {
        override fun areItemsTheSame(oldItem: Subscription, newItem: Subscription): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Subscription, newItem: Subscription): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }

    val differ = AsyncListDiffer(this, differCallback)

    fun submitList(list: List<Subscription>) = differ.submitList(list)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubscriptionViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.item_subscription,
            parent,
           false
        )
        return SubscriptionViewHolder(view)
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    override fun onBindViewHolder(holder: SubscriptionViewHolder, position: Int) {
        val subscription = differ.currentList[position]
        holder.itemView.apply {
            topic.text = subscription.topic
            setOnClickListener {
                onItemClickListener?.let { it(subscription)}
            }
        }
    }

    private var onItemClickListener: ((Subscription) -> Unit)? = null

    fun setOnItemClickListener(listener: (Subscription) -> Unit) {
        onItemClickListener = listener
    }
}
