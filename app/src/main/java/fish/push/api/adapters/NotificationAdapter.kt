package fish.push.api.adapters

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYouListener
import fish.push.api.R
import fish.push.api.db.Notification
import kotlinx.android.synthetic.main.item_notification.view.*
import java.text.SimpleDateFormat
import java.util.*

class NotificationAdapter: RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder>(){
    class NotificationViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    private val differCallback = object : DiffUtil.ItemCallback<Notification>() {
        override fun areItemsTheSame(oldItem: Notification, newItem: Notification): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Notification, newItem: Notification): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }

    val differ = AsyncListDiffer(this, differCallback)

    fun submitList(list: List<Notification>) = differ.submitList(list)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_notification, parent, false)
        return NotificationViewHolder(view)
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    override fun onBindViewHolder(holder: NotificationViewHolder, position: Int) {
        val notification = differ.currentList[position]
        holder.itemView.apply{
            GlideToVectorYou.init().with(this.context).withListener(object: GlideToVectorYouListener {

                override fun onLoadFailed() {
                }

                override fun onResourceReady() {
                }
            })?.load(if (notification.imageURL != ""){Uri.parse(notification.imageURL)} else {Uri.parse("https://push.fish/img/pushfish.svg")}, ivNotificationImage)
            tvContent.text = notification.message
            tvTitle.text = notification.server
            tvTopic.text = notification.topic
            val calendar = Calendar.getInstance().apply {
                timeInMillis = notification.timestamp
            }
            val dateFormat = SimpleDateFormat("MM/dd/yyyy", Locale.getDefault())
            tvDate.text = dateFormat.format(calendar.time)
            setOnClickListener {
                onItemClickListener?.let { it(notification)}
            }
        }
    }

    private var onItemClickListener: ((Notification) -> Unit)? = null

    fun setOnItemClickListener(listener: (Notification) -> Unit) {
        onItemClickListener = listener
    }
}