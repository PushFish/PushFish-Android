package fish.push.api.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import fish.push.api.R
import fish.push.api.db.Connection
import kotlinx.android.synthetic.main.item_connection.view.*


class ConnectionAdapter : RecyclerView.Adapter<ConnectionAdapter.ConnectionViewHolder>() {
    class ConnectionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    private val differCallback = object : DiffUtil.ItemCallback<Connection>() {
        override fun areItemsTheSame(oldItem: Connection, newItem: Connection): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Connection, newItem: Connection): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }

    val differ = AsyncListDiffer(this, differCallback)

    fun submitList(list: List<Connection>) = differ.submitList(list)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ConnectionViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.item_connection,
            parent,
            false
        )
        return ConnectionViewHolder(view)
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    override fun onBindViewHolder(holder: ConnectionViewHolder, position: Int) {
        val connection = differ.currentList[position]
        holder.itemView.apply{
            Glide.with(context)
                .load(connection.imageURL)
                .apply(
                    RequestOptions().dontAnimate().fitCenter()
                )
                .into(ivConnectionImage)
            tvTitle.text = connection.displayName
            setOnClickListener {
                onItemClickListener?.let { it(connection)}
            }
        }
    }

    private var onItemClickListener: ((Connection) -> Unit)? = null

    fun setOnItemClickListener(listener: (Connection) -> Unit) {
        onItemClickListener = listener
    }
}