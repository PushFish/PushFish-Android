package fish.push.api.repositories

import fish.push.api.db.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MainRepository @Inject constructor(
    private val notificationDAO: NotificationDao,
    private val connectionDao: ConnectionDao
) {
    suspend fun addNotification(notification: Notification) {
        notification.id = notificationDAO.addNotification(notification)
    }

    suspend fun deleteNotification(notification: Notification) =
        notificationDAO.deleteNotification(notification)

    fun filterBy(type: String) = notificationDAO.filterBy(type)

    suspend fun numberOfNotifications() = notificationDAO.numberOfNotifications()

    suspend fun addConnection(connection: Connection) {
        connection.id = connectionDao.addConnection(connection)
    }

    suspend fun deleteConnection(connection: Connection) =
        connectionDao.deleteConnection(connection)

    fun getConnections() = connectionDao.getConnections()

    fun getSubscriptions(connection: Connection) = connectionDao.getSubscriptions(connection.id)

    suspend fun subscribe(connection: Connection, subscription: Subscription) {
        subscription.id = connectionDao.subscribe(connection, subscription)
    }

    suspend fun unsubscribe(subscription: Subscription) =
        connectionDao.unsubscribe(subscription)


}