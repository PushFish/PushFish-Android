package fish.push.api

object Constants {
    const val NOTIFICATION_DATABASE_NAME : String =  "notification_db"
    const val ACTION_START_OR_RESUME_SERVICE = "ACTION_START_OR_RESUME_SERVICE"
    const val ACTION_PAUSE_SERVICE = "ACTION_PAUSE_SERVICE"
    const val ACTION_STOP_SERVICE = "ACTION_STOP_SERVICE"
    const val ACTION_SHOW_NOTIFICATION_FRAGMENT = "ACTION_SHOW_NOTIFICATION_FRAGMENT"

    const val NOTIFICATION_CHANNEL_ID = "pushfish_channel"
    const val NOTIFICATION_CHANNEL_NAME = "PushFish"
    const val NOTIFICATION_ID = 1
    const val historyProperty = "history"
    const val ConnectionStatusProperty = "connectionStatus"
    const val empty = ""
    const val CONNECTION_KEY = "CONNECTION_KEY"

    const val CONNECTED = "CONNECTEd"
}