package fish.push.api.mqtt


import android.content.Context
import android.content.Intent
import fish.push.api.Constants
import fish.push.api.R
import fish.push.api.db.Notification
import fish.push.api.db.Subscription
import fish.push.api.services.PushService
import fish.push.api.ui.viewmodels.MainViewModel
import org.eclipse.paho.android.service.MqttAndroidClient
import org.eclipse.paho.client.mqttv3.*
import java.util.*


class ConnectionManager(
    val context: Context,
    private val connectionParams: MQTTConnectionParams,
    val viewModel: MainViewModel
) {

    private var status = ConnectionStatus.NONE
    private val subscriptions: Map<String, Subscription> = HashMap<String, Subscription>()
    private var client = MqttAndroidClient(
        context, connectionParams.host, connectionParams.clientId + id(
            context
        )
    )
    private var uniqueID:String? = null
    private val PREF_UNIQUE_ID = "PREF_UNIQUE_ID"

    init {

        client.setCallback(object : MqttCallbackExtended {
            override fun connectComplete(b: Boolean, s: String) {
                status = ConnectionStatus.CONNECTED
            }

            override fun connectionLost(throwable: Throwable) {
                status = ConnectionStatus.DISCONNECTED
            }

            override fun messageArrived(topic: String, mqttMessage: MqttMessage) {
                val dateTimeStamp = Calendar.getInstance().timeInMillis
                val note = Notification(
                    "",
                    dateTimeStamp,
                    client.serverURI,
                    topic,
                    mqttMessage.toString()
                )
                viewModel.add(note)
            }

            override fun deliveryComplete(iMqttDeliveryToken: IMqttDeliveryToken) {
            }
        })
    }

    /**
     * Attempts to connect client to MQTT broker
     * @return false if connection fails, true otherwise.
     */
    fun connect(username: String, password: String, spProtocol: Int): Boolean{
        var didConnectionSucceed = false
        val mqttConnectOptions = MqttConnectOptions()
        if (username.isNotEmpty()) {
            mqttConnectOptions.userName = username
        }
        if (password.isNotEmpty()) {
            mqttConnectOptions.password = password.toCharArray()
        }
        mqttConnectOptions.isAutomaticReconnect = true
        if (spProtocol == 1) { // SSL
            val socketFactoryOptions = SocketFactory.SocketFactoryOptions()
            try {
                socketFactoryOptions.withCaInputStream(
                    context.resources
                        .openRawResource(R.raw.chain)
                )
                mqttConnectOptions.socketFactory = SocketFactory(
                    socketFactoryOptions
                )
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        try
        {
            status = ConnectionStatus.CONNECTING
            client.connect(mqttConnectOptions, null, object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken) {
                    val disconnectedBufferOptions = DisconnectedBufferOptions()
                    disconnectedBufferOptions.isBufferEnabled = true
                    disconnectedBufferOptions.bufferSize = 100
                    disconnectedBufferOptions.isPersistBuffer = false
                    disconnectedBufferOptions.isDeleteOldestMessages = false
                    client.setBufferOpts(disconnectedBufferOptions)
                    val intent = Intent(context, PushService::class.java)
                    intent.action = Constants.ACTION_START_OR_RESUME_SERVICE
                    context.startService(intent)
                    didConnectionSucceed = true
                    status = ConnectionStatus.CONNECTED
                }

                override fun onFailure(asyncActionToken: IMqttToken, exception: Throwable) {
                    throw exception
                }
            })
        }
        catch (ex: MqttException) {
            ex.printStackTrace()
            return didConnectionSucceed
        }
        return didConnectionSucceed
    }

    fun disconnect(){
        try {
            client.disconnect()
        }
        catch (ex: MqttException) {
            System.err.println("Exception disconnect")
            ex.printStackTrace()
        }
    }

    // Subscribe to topic
    fun subscribe(topic: String, qos : Int){
        try
        {
            client.subscribe(topic, qos, null, object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken) {
                }

                override fun onFailure(asyncActionToken: IMqttToken, exception: Throwable) {
                }
            })
        }
        catch (ex: MqttException) {
            System.err.println("Exception subscribing")
            ex.printStackTrace()
        }
    }

    fun unsubscribe(topic: String){
        try {
            client.unsubscribe(topic)
        }
        catch (ex: MqttException) {
            System.err.println("Exception unsubscribe")
            ex.printStackTrace()
        }

    }

    fun publish(topic: String, message: String){
        try
        {
            client.publish(topic, message.toByteArray(), 0, false)
        }
        catch (ex: MqttException) {
            System.err.println("Exception publishing")
            ex.printStackTrace()
        }
    }

    @Synchronized fun id(context: Context):String {
        if (uniqueID == null)
        {
            val sharedPrefs = context.getSharedPreferences(
                PREF_UNIQUE_ID, Context.MODE_PRIVATE
            )
            uniqueID = sharedPrefs.getString(PREF_UNIQUE_ID, null)
            if (uniqueID == null)
            {
                uniqueID = UUID.randomUUID().toString()
                val editor = sharedPrefs.edit()
                editor.putString(PREF_UNIQUE_ID, uniqueID)
                editor.apply()
            }
        }
        return uniqueID!!
    }
}

/**
 * Client Connection Status
 */
enum class ConnectionStatus {
    CONNECTING,
    CONNECTED,
    DISCONNECTING,
    DISCONNECTED,
    ERROR,
    NONE
}

data class MQTTConnectionParams(
    val clientId: String,
    val host: String,
    val topic: String,
    val username: String,
    val password: String
)

