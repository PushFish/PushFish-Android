package fish.push.api.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.NotificationManager.IMPORTANCE_LOW
import android.app.PendingIntent
import android.app.PendingIntent.FLAG_UPDATE_CURRENT
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import dagger.hilt.android.AndroidEntryPoint
import fish.push.api.Constants.ACTION_PAUSE_SERVICE
import fish.push.api.Constants.ACTION_START_OR_RESUME_SERVICE
import fish.push.api.Constants.ACTION_STOP_SERVICE
import fish.push.api.Constants.NOTIFICATION_CHANNEL_ID
import fish.push.api.Constants.NOTIFICATION_CHANNEL_NAME
import fish.push.api.Constants.NOTIFICATION_ID
import fish.push.api.R
import timber.log.Timber
import javax.inject.Inject

@AndroidEntryPoint
class PushService : Service(){
    private var isInitialStart = true
    private var isServiceActive = false
    @Inject
    lateinit var baseNotificationBuilder : NotificationCompat.Builder

    lateinit var curNotificationBuilder : NotificationCompat.Builder

    override fun onCreate() {
        super.onCreate()
        curNotificationBuilder = baseNotificationBuilder
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        intent?.let {
            when(it.action) {
                ACTION_START_OR_RESUME_SERVICE -> {
                    if(isInitialStart){
                        startForegroundService()
                        isInitialStart = false
                        Timber.d("Started service...")
                    } else {
                        Timber.d("Resumed service...")
                    }
                }

                ACTION_PAUSE_SERVICE -> {
                    Timber.d("Paused service.")
                    pauseService()
                }

                ACTION_STOP_SERVICE -> {
                    Timber.d("Stopped service!")
                    killService()
                }
            }
        }
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onBind(p0: Intent?): IBinder? { return null }

    private fun startForegroundService() {
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE)
            as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel(notificationManager)
            Timber.d("Created Notification channel!")
        }

        startForeground(NOTIFICATION_ID, baseNotificationBuilder.build())
        Timber.d("Started Foreground Service!")
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(notificationManager: NotificationManager) {
        val channel = NotificationChannel(
            NOTIFICATION_CHANNEL_ID,
            NOTIFICATION_CHANNEL_NAME,
            IMPORTANCE_LOW
        )
        notificationManager.createNotificationChannel(channel)
    }

    private fun updateNotificationState(isListening: Boolean) {
        val notificationActionText = if(isListening) "Pause" else "Resume"
        val pendingIntent = if(isListening){
            val pauseIntent = Intent(this, PushService::class.java).apply {
                action = ACTION_PAUSE_SERVICE
            }
            PendingIntent.getService(this, 1, pauseIntent, FLAG_UPDATE_CURRENT)
        } else {
            val resumeIntent = Intent(this, PushService::class.java).apply {
                action = ACTION_START_OR_RESUME_SERVICE
            }
            PendingIntent.getService(this, 2, resumeIntent, FLAG_UPDATE_CURRENT)
        }
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        curNotificationBuilder.javaClass.getDeclaredField("mAction").apply {
            isAccessible = true
            set(curNotificationBuilder, ArrayList<NotificationCompat.Action>())
        }
        curNotificationBuilder = baseNotificationBuilder
            .addAction(R.drawable.ic_baseline_pause_24, notificationActionText, pendingIntent)
        notificationManager.notify(NOTIFICATION_ID, curNotificationBuilder.build())
    }

    private fun killService() {
        isServiceActive = false
        isInitialStart = true
        pauseService()
        stopForeground(true)
        stopSelf()
    }

    private fun pauseService() {
        isServiceActive = false
    }
}