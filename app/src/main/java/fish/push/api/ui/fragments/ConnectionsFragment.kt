package fish.push.api.ui.fragments

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import fish.push.api.PushFishApplication
import fish.push.api.R
import fish.push.api.adapters.ConnectionAdapter
import fish.push.api.internal.Connections
import fish.push.api.ui.viewmodels.MainViewModel
import kotlinx.android.synthetic.main.fragment_connections.*

@AndroidEntryPoint
class ConnectionsFragment : Fragment(R.layout.fragment_connections) {

    private val viewModel: MainViewModel by viewModels()
    private lateinit var connectionAdapter: ConnectionAdapter
    private var connections = PushFishApplication.connections

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()

        viewModel.connections.observe(viewLifecycleOwner, {
            connectionAdapter.submitList(it)
        })

        connectionAdapter.setOnItemClickListener {
            val bundle = Bundle().apply {
                putSerializable("connection", it)
            }
            findNavController().navigate(
                R.id.action_connectionsFragment_to_manageConnectionFragment,
                bundle
            )
        }

        val itemTouchHelperCallback = object : ItemTouchHelper.SimpleCallback(
            ItemTouchHelper.UP or ItemTouchHelper.DOWN,
            ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
        ) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return true
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.adapterPosition
                val connection = connectionAdapter.differ.currentList[position]
                viewModel.delete(connection)
                Snackbar.make(view, "Successfully disconnected", Snackbar.LENGTH_LONG).apply {
                    setAction("Undo"){
                        viewModel.add(connection)
                    }
                    show()
                }
            }
        }

        ItemTouchHelper(itemTouchHelperCallback).apply {
            attachToRecyclerView(rvConnections)
        }
    }
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.toolbar_connections_menu, menu)
        menu.findItem(R.id.item_add_connection).setOnMenuItemClickListener {
            findNavController().navigate(R.id.action_connectionsFragment_to_addConnectionsFragment)
            true
        }
        super.onCreateOptionsMenu(menu, inflater)
    }

    private fun setupRecyclerView() = rvConnections.apply{
        connectionAdapter = ConnectionAdapter()
        adapter = connectionAdapter
        layoutManager = LinearLayoutManager(requireContext())
    }
}
