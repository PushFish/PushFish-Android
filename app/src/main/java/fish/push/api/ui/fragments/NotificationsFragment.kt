package fish.push.api.ui.fragments

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.qualifiers.ApplicationContext
import fish.push.api.PushFishApplication
import fish.push.api.R
import fish.push.api.adapters.NotificationAdapter
import fish.push.api.other.SortType
import fish.push.api.ui.viewmodels.MainViewModel
import kotlinx.android.synthetic.main.fragment_notifications.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber

@AndroidEntryPoint
class NotificationsFragment : Fragment(R.layout.fragment_notifications) {

    private val viewModel: MainViewModel by viewModels()
    private lateinit var notificationAdapter: NotificationAdapter
    private var connections = PushFishApplication.connections
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()

        when(viewModel.sortType){
            SortType.TIMESTAMP -> spFilter.setSelection(0)
            SortType.SERVER -> spFilter.setSelection(1)
            SortType.TOPIC -> spFilter.setSelection(2)
        }

        spFilter.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected( adapterView: AdapterView<*>, view: View?, position: Int, id: Long ) {
                when (position) {
                    0-> viewModel.sortNotifications(SortType.TIMESTAMP)
                    1-> viewModel.sortNotifications(SortType.SERVER)
                    2-> viewModel.sortNotifications(SortType.TOPIC)
                }
            }
        }
        viewModel.notifications.observe(viewLifecycleOwner, {
            notificationAdapter.submitList(it)
        })

        val itemTouchHelperCallback = object : ItemTouchHelper.SimpleCallback(
            ItemTouchHelper.UP or ItemTouchHelper.DOWN,
            ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
        ) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return true
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.adapterPosition
                val notification = notificationAdapter.differ.currentList[position]
                viewModel.delete(notification)
                Snackbar.make(view, "Successfully deleted notification", Snackbar.LENGTH_LONG).apply {
                    setAction("Undo"){
                        viewModel.add(notification)
                        setViewsBasedOnRepo()
                    }
                    show()
                }
                setViewsBasedOnRepo()
            }
        }

        ItemTouchHelper(itemTouchHelperCallback).apply {
            attachToRecyclerView(rvNotifications)
        }

        btnAddSubscription.setOnClickListener {
            findNavController().navigate(R.id.action_miNotifications_to_miSubscriptions)
        }

    }

    override fun onResume() {
        super.onResume()
        setViewsBasedOnRepo()
    }
    private fun setViewsBasedOnRepo() {
        GlobalScope.launch(Dispatchers.Main) {
            val size  =
                withContext(Dispatchers.Default) { viewModel.databaseNumberOfNotifications() }
            if (size == 0) {
                btnAddSubscription.visibility = View.VISIBLE
                tvNoNotifications.visibility = View.VISIBLE
                spFilter.visibility = View.INVISIBLE
                tvSortBy.visibility = View.INVISIBLE
                rvNotifications.visibility = View.INVISIBLE
            } else {
                btnAddSubscription.visibility = View.INVISIBLE
                tvNoNotifications.visibility = View.INVISIBLE
                spFilter.visibility = View.VISIBLE
                tvSortBy.visibility = View.VISIBLE
                rvNotifications.visibility = View.VISIBLE
            }
        }
    }
    private fun setupRecyclerView() = rvNotifications.apply {
        notificationAdapter = NotificationAdapter()
        adapter = notificationAdapter
        layoutManager = LinearLayoutManager(requireContext())
    }
}