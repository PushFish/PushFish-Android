package fish.push.api.ui.fragments

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SwitchCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import fish.push.api.R
import fish.push.api.adapters.SubscriptionAdapter
import fish.push.api.db.Connection
import fish.push.api.db.Subscription
import fish.push.api.ui.viewmodels.MainViewModel
import kotlinx.android.synthetic.main.fragment_manageconnection.*

@AndroidEntryPoint
class ManageConnectionFragment : Fragment(R.layout.fragment_manageconnection) {
    private val viewModel : MainViewModel by viewModels()
    private lateinit var subscriptionAdapter: SubscriptionAdapter
    private val args: ManageConnectionFragmentArgs by navArgs()
    private lateinit var connection : Connection

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        connection = args.connection
        viewModel.getSubscriptions(connection).observe(viewLifecycleOwner, {
            subscriptionAdapter.submitList(it)
        })
        (activity as AppCompatActivity).supportActionBar?.title = connection.displayName
        btnSubscribe.setOnClickListener {
            if (etTopicToSubscibe.text?.isNotEmpty()!!) {
                val subscription = Subscription(
                    connection.id,
                    connection.clientHandle,
                    etTopicToSubscibe.text.toString()
                )
                viewModel.subscribe(connection, subscription)
            }
        }

        val itemTouchHelperCallback = object : ItemTouchHelper.SimpleCallback(
            ItemTouchHelper.UP or ItemTouchHelper.DOWN,
            ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
        ) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return true
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.adapterPosition
                val subscription = subscriptionAdapter.differ.currentList[position]
                viewModel.unsubscribe(subscription)
                Snackbar.make(view, "Successfully unsubscribed ", Snackbar.LENGTH_LONG).apply {
                    setAction("Undo"){
                        viewModel.subscribe(connection, subscription)
                    }
                    show()
                }
            }
        }

        ItemTouchHelper(itemTouchHelperCallback).apply {
            attachToRecyclerView(rvSubscriptions)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.toolbar_manageconnection_menu, menu)
        val item = menu.findItem(R.id.app_bar_switch)
        item.setActionView(R.layout.switch_layout)
        val switch = item.actionView.findViewById<SwitchCompat>(R.id.toggleConnection)
        switch.setOnCheckedChangeListener { _, isChecked ->
            toggleConnection(isChecked)
        }
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.toggleConnection ->
                return true
        }
        return super.onOptionsItemSelected(item)
    }
    private fun toggleConnection(isChecked : Boolean)
    {
    }

    private fun setupRecyclerView() = rvSubscriptions.apply {
        subscriptionAdapter = SubscriptionAdapter()
        adapter = subscriptionAdapter
        layoutManager = LinearLayoutManager(requireContext())
    }
}