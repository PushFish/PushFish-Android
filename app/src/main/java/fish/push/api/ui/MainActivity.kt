package fish.push.api.ui

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.navigation.NavigationView
import dagger.hilt.android.AndroidEntryPoint
import fish.push.api.Constants
import fish.push.api.Constants.ACTION_SHOW_NOTIFICATION_FRAGMENT
import fish.push.api.R
import fish.push.api.services.PushService
import fish.push.api.ui.viewmodels.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.eclipse.paho.client.mqttv3.MqttConnectOptions
import java.beans.PropertyChangeEvent
import java.beans.PropertyChangeListener

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var appBarConfiguration: AppBarConfiguration
    lateinit var toggle: ActionBarDrawerToggle
    private val viewModel: MainViewModel by viewModels()

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        navigateToNotificationFragmentIfNeeded(intent)
        toggle = ActionBarDrawerToggle(this, drawer_layout,
            R.string.open,
            R.string.close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        setSupportActionBar(toolbar)
        val navController = findNavController(R.id.nav_host_fragment)
        val topLevelDestinations = setOf(R.id.notificationsFragment, R.id.connectionsFragment)
        appBarConfiguration = AppBarConfiguration(topLevelDestinations, drawer_layout)
        findViewById<NavigationView>(R.id.nav_view)
            .setupWithNavController(navController)
        setupActionBarWithNavController(navController, appBarConfiguration)
        toolbar.setupWithNavController(navController, appBarConfiguration)
        startService(Intent(this, PushService::class.java))

        viewModel.databaseConnections().observe(this, {connections ->
            connections?.let {
                viewModel.initConnections(it,this)
            }
        })
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        navigateToNotificationFragmentIfNeeded(intent)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId)
        {
            R.id.item_add_connection ->
                return true
            R.id.toggleConnection ->
                return false
        }
        if (toggle.onOptionsItemSelected(item)) {
            return true
        }
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration)
                || super.onSupportNavigateUp()
    }

    private fun navigateToNotificationFragmentIfNeeded(intent: Intent?){
        if (intent?.action == ACTION_SHOW_NOTIFICATION_FRAGMENT) {
            nav_host_fragment.findNavController().navigate(R.id.action_global_notificationFragment)
        }
    }

    private class ChangeListener : PropertyChangeListener {
        override fun propertyChange(event: PropertyChangeEvent) {
            if (event.propertyName != Constants.ConnectionStatusProperty) {
                return
            }
            // Runnable { navController.notifyDataSetChanged() })
        }
    }
}