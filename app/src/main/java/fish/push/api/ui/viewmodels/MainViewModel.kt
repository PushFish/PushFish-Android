package fish.push.api.ui.viewmodels

import android.content.Context
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fish.push.api.db.Connection
import fish.push.api.db.Notification
import fish.push.api.db.Subscription
import fish.push.api.mqtt.MQTTConnectionParams
import fish.push.api.mqtt.ConnectionManager
import fish.push.api.other.SortType
import fish.push.api.repositories.MainRepository
import kotlinx.coroutines.launch

class MainViewModel @ViewModelInject constructor(
    private val repository: MainRepository
): ViewModel() {

    private val notificationsSortedByTimeStamp = filterBy(type = "timestamp")
    private val notificationsSortedByServer = filterBy(type = "server")
    private val notificationsSortedByTopic = filterBy(type = "topic")

    val notifications = MediatorLiveData<List<Notification>>()
    val connections = repository.getConnections()

    var sortType = SortType.TIMESTAMP

    init {
        notifications.addSource(notificationsSortedByTimeStamp) { result ->
            if (sortType == SortType.TIMESTAMP) {
                result?.let { notifications.value = it }
            }
        }
        notifications.addSource(notificationsSortedByServer) { result ->
            if (sortType == SortType.SERVER) {
                result?.let { notifications.value = it }
            }
        }
        notifications.addSource(notificationsSortedByTopic) { result ->
            if (sortType == SortType.TOPIC) {
                result?.let { notifications.value = it }
            }
        }
    }

    fun add(item: Notification) = viewModelScope.launch {
        repository.addNotification(item)
    }

    fun delete(item: Notification) = viewModelScope.launch {
        repository.deleteNotification(item)
    }

    fun add(item: Connection) = viewModelScope.launch {
        repository.addConnection(item)
    }

    fun delete(item: Connection) = viewModelScope.launch {
        repository.deleteConnection(item)
    }

    fun filterBy(type: String) = repository.filterBy(type)

    fun sortNotifications(sortType: SortType) = when (sortType) {
        SortType.TIMESTAMP -> notificationsSortedByTimeStamp.value?.let { notifications.value = it }
        SortType.SERVER -> notificationsSortedByServer.value?.let { notifications.value = it }
        SortType.TOPIC -> notificationsSortedByTopic.value?.let {
            notifications.value = it
        }
    }.also {
        this.sortType = sortType
    }

    suspend fun databaseNumberOfNotifications() : Int {
        return repository.numberOfNotifications()
    }

    fun databaseConnections() = repository.getConnections()

    fun getSubscriptions(connection: Connection) : LiveData<List<Subscription>> =
        repository.getSubscriptions(connection)

    fun subscribe(connection: Connection, subscription: Subscription) = viewModelScope.launch {
        repository.subscribe(connection,subscription)
    }

    fun unsubscribe(item: Subscription) = viewModelScope.launch {
        repository.unsubscribe(item)
    }

    fun initConnections(connections: List<Connection>,context: Context) {
        connections.forEach {  connection ->
            if (connection.shouldConnect) {
                val mqttConnectionParams = MQTTConnectionParams(
                    connection.clientID,
                    connection.host,
                    connection.lwtTopic,
                    connection.username,
                    connection.password
                )
                val mqttManager = ConnectionManager(context, mqttConnectionParams, this)
                mqttManager.connect(connection.username, connection.password, connection.ssl)
            }
        }
    }
}

