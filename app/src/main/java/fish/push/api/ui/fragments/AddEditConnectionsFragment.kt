package fish.push.api.ui.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Activity.RESULT_OK
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.MediaStore
import android.view.*
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import fish.push.api.PushFishApplication
import fish.push.api.R
import fish.push.api.db.Connection
import fish.push.api.ui.viewmodels.MainViewModel
import kotlinx.android.synthetic.main.fragment_addeditconnection.*
import timber.log.Timber
import java.util.*


@AndroidEntryPoint
class AddEditConnectionsFragment : Fragment(R.layout.fragment_addeditconnection) {
    private val viewModel: MainViewModel by viewModels()
    private var connections = PushFishApplication.connections

    // Storage Permissions
    private val REQUEST_EXTERNAL_STORAGE = 1
    private val PERMISSIONS_STORAGE = arrayOf(
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )

    companion object {
        // Used to append a random suffix to android client's on MQTT brokers
        private const val AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        private val random = Random()
        private const val length = 8
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.toolbar_addeditconnection_menu, menu)
        menu.findItem(R.id.item_confirm_connection).setOnMenuItemClickListener {
            val connection = buildConnection()
            if (connection != null)
            {
                addEditConnection(connection)
                findNavController().navigate(R.id.action_addConnectionsFragment_to_connectionsFragment)
            }
            true
        }
        super.onCreateOptionsMenu(menu, inflater)
    }

    /**
     * Helper function to verify connections are VALID before adding them to the
     * Room database.
     *
     * @return true if none of the INVALID criterion are met, otherwise false.
     * @return false should popup an AlertDialog with reasoning for why the
     * connection is INVALID
     */
    private fun isValidConnection() : Boolean
    {
        var isValid = true
        val builder : AlertDialog.Builder? = activity?.let {
            AlertDialog.Builder(it)
        }

        if (port.text.toString() == "1883" && spProtocol.selectedItem == "ssl") {
            isValid = false
            builder?.setTitle("Use port 8883 for TLS/SSL connections!")
        }
        if (port.text.toString() == "8883" && spProtocol.selectedItem == "tcp") {
            isValid = false
            builder?.setTitle("Port 8883 is reserved for TLS/SSL!")
        }
        if (!isValid) {
            builder?.setPositiveButton("OK") { _, _ -> }
            builder?.show()
            return false
        }
        return true
    }

    /**
     * Adds, or updates, the given connection object in the database as well as the
     * Push Fish application's live connection list.
     */
    private fun addEditConnection(connection: Connection) {
        connections.addConnection(connection)
        viewModel.add(connection)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_addeditconnection, container, false)
    }

    /**
     * Builds a Connection entity using this fragment's resource values
     *
     * @return a Connection that may be invalid. If returned Connection would be invalid,
     * returns null instead.
     */
    @SuppressLint("SetTextI18n")
    private fun buildConnection() : Connection? {
        // Generate a randomized client handle suffix
        val sb = StringBuilder(length)
        for (i in 0 until length) {
            sb.append(
                AB[random.nextInt(AB.length)]
            )
        }
        val clientHandle = client_id.text.toString() + sb.toString()

        var hostNameUrl = hostname.text.toString()

        // If user specified protocol/port in the URL
        var protocolSpecified = false
        if(hostNameUrl.startsWith("tcp://")){
            spProtocol.setSelection(0)
            protocolSpecified = true
        } else if (hostNameUrl.startsWith("ssl://")){
            spProtocol.setSelection(1)
            protocolSpecified = true
        }

        // Otherwise use the spinner protocol to specify which protocol to use
        if (!protocolSpecified){
            // Use the spinner protocol to define which protocol to use
            hostNameUrl= "${spProtocol.selectedItem}://$hostNameUrl"
        }

        if (displayName.text.isNullOrEmpty()) {
            displayName.text = hostname.text
        }

        if (!isValidConnection())
            return null

        return Connection(
            clientHandle,
            hostNameUrl,
            displayName.text.toString(),
            imageURL.text.toString(),
            client_id.text.toString(),
            port.text.toString().toInt(),
            spProtocol.selectedItemId.toInt(),
            timeout.text.toString().toInt(),
            keepalive.text.toString().toInt(),
            username.text.toString(),
            password.text.toString(),
            if (clean_session_switch.isChecked) {
                1
            } else {
                0
            },
            lwt_topic.text.toString(),
            lwt_message.text.toString(),
            lwt_qos_spinner.selectedItem.toString().toInt(),
            if (retain_switch.isChecked) {
                1
            } else {
                0
            }
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bBrowseImage.setOnClickListener {
            val hasPermission = verifyStoragePermissions(requireActivity())

            if (hasPermission) {
                val getIntent = Intent(Intent.ACTION_GET_CONTENT)
                getIntent.type = "image/*"

                val pickIntent = Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                )
                pickIntent.type = "image/*"

                val chooserIntent = Intent.createChooser(getIntent, "Select Image")
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, arrayOf(pickIntent))

                startActivityForResult(chooserIntent, 1)
            }
        }
    }

    @Suppress("DEPRECATION")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == RESULT_OK && data!= null)
        {
            val imageCaptureURI = data.data
            val projection = arrayOf(
                MediaStore.Images.Media.DATA,
            )
            val resolver = requireContext().contentResolver
            if (imageCaptureURI != null) {
                resolver.query(
                    imageCaptureURI,
                    projection,
                    null, null, null
                )?.use {cursor ->
                    cursor.moveToFirst()
                    val displayIndex = cursor.getColumnIndex(MediaStore.Images.Media.DATA)
                    val displayPath: String? = displayIndex.let { cursor.getString(it) }
                    imageURL.setText(displayPath)
                }
            }
        }
    }

    /**
     * Checks if the app has permission to write to device storage
     *
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity
     */
    private fun verifyStoragePermissions(activity: Activity) : Boolean {
        // Check if we have write permission
        val permission = ActivityCompat.checkSelfPermission(
            activity,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                activity,
                PERMISSIONS_STORAGE,
                REQUEST_EXTERNAL_STORAGE
            )
            return false
        }
        return true
    }
}