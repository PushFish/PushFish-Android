The PushFish Android Client [![License](http://img.shields.io/badge/license-BSD-blue.svg?style=flat)](/LICENSE)
==========================
This is the PushFish Android client. It's using MQTT to communicate with the PushFish server. The client is licensed
under [BSD 2 clause][1] just like the rest of the project.

The concept is based loosely on ![Pushjet](https://github.com/Pushjet/Pushjet-Android), but the project itself is written without any reference to Pushjet.

![Download latest apk](https://gitlab.com/PushFish/PushFish-Android/-/jobs/artifacts/master/raw/app/build/outputs/apk/app-debug.apk?job=build)
## Permissions explained
The [permissions][4] used by Pushfish might seem a bit broad but they all have a reason:

 - Read phone status and identity:
  - This is needed [to generate the device uuid ][5] that authenticates the device with the server.
 - Take pictures and videos:
  - This is needed to make sure we can scan QR codes to register new services.
 - Control flashlight/vibration and prevent phone from sleeping:
  - This makes sure we can receive notifications.

## Screenshots

## TODO
 * Get Screenshots of application
 * Add notification actions
 * Handle autostart and battery optimization
 * Add application prefrences
 * Handle websocket connection
 
[1]: https://tldrlegal.com/license/bsd-2-clause-license-%28freebsd%29
[4]: /app/src/main/AndroidManifest.xml
[5]: https://github.com/Pushjet/Pushjet-Android/blob/master/app/src/main/java/io/Pushjet/api/PushjetApi/DeviceUuidFactory.java
